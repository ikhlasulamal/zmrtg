#!/usr/bin/env python

import tempfile
import shutil

class FixEthFile:
    """Perbaiki saja EthFile"""

    def __init__(self, fname):
        self.filename = fname       

    def fix(self):
        fin = open(self.filename, 'r')
        ftmp = tempfile.NamedTemporaryFile(delete=False)
        tmpname = ftmp.name

        # first line
        line = fin.readline()
        field = line.split()
        timef = int(field[0])
        timef += 300
        field[0] = str(timef)
        line = ' '.join(field)
        ftmp.write(line + '\n')

        # second line
        line = field[0] + ' 0 0 0 0\n'
        ftmp.write(line)

        # the rest is written as is
        for line in fin:
            ftmp.write(line)

        # close two files
        fin.close()
        ftmp.close()

        shutil.move(tmpname, self.filename)

if __name__ == "__main__":
    fix = FixEthFile('/var/www/mrtg/localhost_eth0.log')
    fix.fix()

    fix = FixEthFile('/var/www/mrtg/localhost_eth1.log')
    fix.fix()

# vim: set tabstop=4 shiftwidth=4 expandtab :
